package com.example.Security.impl;

import com.example.Security.models.User;
import com.example.Security.repository.UserRepository;
import com.example.Security.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;


    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Override
    public List<User> getAllusers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        User user3 = userRepository.findById(id).orElse(null);
        return user3;
    }

    @Override
    public void updateUser(Long id, User user) {
        user.setId(id);
        userRepository.save(user);
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }}

//    @Override
//    public String[] getAlltextFromMessageInDate() {
//        return userRepository.getAlltextFromMessageInDate();
//    }
//    @Override
//    public String[] getAlltextFromMessageInDateOfUser(){
//        return userRepository.getAlltextFromMessageInDateOfUser();
//
//    }}



