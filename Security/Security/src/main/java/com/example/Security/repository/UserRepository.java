package com.example.Security.repository;

import com.example.Security.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

//    @Query(value = "SELECT text FROM messages WHERE date_time = '2023-06-05' ", nativeQuery = true)
//    String[] getAlltextFromMessageInDate();
//
//    @Query(value = "SELECT text FROM messages WHERE (date_time = '2023-06-05' AND sender = 'Mirovaya')", nativeQuery = true)
//    String[] getAlltextFromMessageInDateOfUser();
//
   //Optional<User> findByUserName(String username);
    Optional<User> findByEmail(String email);


}
