package com.example.Security.service;

import com.example.Security.models.Message;

import java.util.List;

public interface MessageService {

    Message createMessage(Message message);

    List<Message> getAllmessages();

    Message getMessageById(Long id);

    void updateMessage(Long id, Message message);

    void deleteMessageById(Long id);




}
