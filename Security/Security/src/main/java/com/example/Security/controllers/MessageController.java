package com.example.Security.controllers;

import com.example.Security.models.Message;
import com.example.Security.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/messages")

public class MessageController {

    public MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/add")
    public Message createMessage(@RequestBody Message message) {
        Message message1 =messageService.createMessage(message);
        return message1;

    }



//    @PostMapping("/add")
//    public Message createMessage(@RequestBody Message message) {
//        messageService.createMessage(message);
//        return message;
//
//    }



    //    @PostMapping("/add")
//    public ResponseEntity<?> addMessage(@RequestBody Message message) {
//        try {
//            Message message1 = new Message();
//            messageService.createMessage(message);
//            return new ResponseEntity<>(message, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>("Не удалось создать сообщение", HttpStatus.BAD_REQUEST);
//        }
//
//    }

    @GetMapping("/all")
    public List<Message> getAllMessages() {
        return messageService.getAllmessages();
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getMessageById(@PathVariable(name = "id") Long id) {
        try {
            Message message = messageService.getMessageById(id);
            if (message == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(message, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удалось получить сообщение по ID", HttpStatus.BAD_REQUEST);
        }

    }


    @PatchMapping("/{id}")
    public void updateMessage(@PathVariable(name = "id") Long id, @RequestBody Message message) {
        messageService.updateMessage(id, message);
    }

    @DeleteMapping("/{id}")
    public void deleteMessageById(@PathVariable(name = "id") Long id) {
        messageService.deleteMessageById(id);
    }



}
