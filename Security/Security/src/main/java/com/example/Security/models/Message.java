package com.example.Security.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "messages")
public class Message  {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "text")
    private String text;

    @CreatedDate
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateTime")
    private Date dateTime;


    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "sender_id")

    private User sender;

    @ManyToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "receiver_id")

    private User receiver;

    public Message() {
    }

    public Message(Long id, String text, User sender, User receiver) {
        this.id = id;
        this.text = text;
        this.sender = sender;
        this.receiver = receiver;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", dateTime=" + dateTime +
                ", sender=" + sender +
                ", receiver=" + receiver +
                '}';
    }
}
