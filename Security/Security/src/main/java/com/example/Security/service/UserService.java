package com.example.Security.service;

import com.example.Security.models.User;

import java.util.List;

public interface UserService {

    void createUser(User user);

    List<User> getAllusers();

    User getUserById(Long id);

    void updateUser(Long id, User user);

    void deleteUserById(Long id);



//    String[] getAlltextFromMessageInDate();
//
//    String[] getAlltextFromMessageInDateOfUser();

}
