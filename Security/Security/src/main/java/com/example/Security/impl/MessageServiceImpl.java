package com.example.Security.impl;

import com.example.Security.models.Message;
import com.example.Security.repository.MessageRepository;
import com.example.Security.service.MessageService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    public MessageRepository messageRepository;

    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Message createMessage(Message message) {
        messageRepository.save(message);
        return message;
    }

    @Override
    public List<Message> getAllmessages() {
        return messageRepository.findAll();
    }

    @Override
    public Message getMessageById(Long id) {
        Message message3 = messageRepository.findById(id).orElse(null);
        return message3;
    }

    @Override
    public void updateMessage(Long id, Message message) {
        message.setId(id);
        messageRepository.save(message);
    }

    @Override
    public void deleteMessageById(Long id) {
        messageRepository.deleteById(id);
    }}


