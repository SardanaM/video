package com.example.Security.controllers;

import com.example.Security.models.User;
import com.example.Security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    //добавить юзера
    @PostMapping(value = "/add", consumes = {"application/json"})
    public User addUsers(@RequestBody User user) {
        userService.createUser(user);
        return user;
    }


    //получить список юзеров
    @GetMapping("/all")
    public List<User> getAllusers() {
        return userService.getAllusers();
    }




    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable(name = "id") Long id) {

        try {
            User user = userService.getUserById(id);
            if (user == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удалось найти пользователя по ID", HttpStatus.BAD_REQUEST);
        }

    }



    @PatchMapping("/{id}")
    public void updateUser(@PathVariable(name = "id") Long id, @RequestBody User user) {
        userService.updateUser(id, user);
    }

    @DeleteMapping("/{id}")
    public void deleteMessageById(@PathVariable(name = "id") Long id) {
        userService.deleteUserById(id);
    }


}
